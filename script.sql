USE [master]
GO
/****** Object:  Database [dbFitnessClub]    Script Date: 20.03.2021 19:37:06 ******/
CREATE DATABASE [dbFitnessClub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbFitnessClub', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbFitnessClub.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbFitnessClub_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbFitnessClub_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbFitnessClub] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbFitnessClub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbFitnessClub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbFitnessClub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbFitnessClub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbFitnessClub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbFitnessClub] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbFitnessClub] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbFitnessClub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbFitnessClub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbFitnessClub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbFitnessClub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbFitnessClub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbFitnessClub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbFitnessClub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbFitnessClub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbFitnessClub] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbFitnessClub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbFitnessClub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbFitnessClub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbFitnessClub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbFitnessClub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbFitnessClub] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbFitnessClub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbFitnessClub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbFitnessClub] SET  MULTI_USER 
GO
ALTER DATABASE [dbFitnessClub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbFitnessClub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbFitnessClub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbFitnessClub] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbFitnessClub] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbFitnessClub] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [dbFitnessClub] SET QUERY_STORE = OFF
GO
USE [dbFitnessClub]
GO
/****** Object:  Table [dbo].[Abonement]    Script Date: 20.03.2021 19:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Abonement](
	[id_abonement] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Stoimost] [money] NULL,
	[Opisanie] [nvarchar](500) NULL,
	[Srok_(days)] [int] NULL,
 CONSTRAINT [PK_Abonement] PRIMARY KEY CLUSTERED 
(
	[id_abonement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Klient]    Script Date: 20.03.2021 19:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Klient](
	[id_klient] [int] IDENTITY(1,1) NOT NULL,
	[FIO] [nvarchar](100) NULL,
	[Data_rozhdeniya] [date] NULL,
 CONSTRAINT [PK_Klient] PRIMARY KEY CLUSTERED 
(
	[id_klient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Klub_Karta]    Script Date: 20.03.2021 19:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Klub_Karta](
	[id_klub] [int] IDENTITY(1,1) NOT NULL,
	[id_klient_FK] [int] NOT NULL,
	[id_abonement_FK] [int] NOT NULL,
	[Data_nachala] [date] NOT NULL,
 CONSTRAINT [PK_Klub_Karta] PRIMARY KEY CLUSTERED 
(
	[id_klub] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Abonement] ON 

INSERT [dbo].[Abonement] ([id_abonement], [Name], [Stoimost], [Opisanie], [Srok_(days)]) VALUES (1, N'ФИТНЕС ГОД', 15000.0000, N'СРОК ДЕЙСТВИЯ 10 МЕС. + 1 ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА.

ВРЕМЯ ПОСЕЩЕНИЯ В БУДНИ С 7.00 ДО 22.00, В ВЫХОДНЫЕ С 9.00 ДО 21.00

КОЛИЧЕСТВО ПОСЕЩЕНИЙ НЕ ОГРАНИЧЕНО В ТЕЧЕНИИ ВСЕГО ДЕЙСТВИЯ АБОНЕМЕНТА.', 365)
INSERT [dbo].[Abonement] ([id_abonement], [Name], [Stoimost], [Opisanie], [Srok_(days)]) VALUES (2, N'ФИТНЕС СЕЗОН', 6150.0000, N'СРОК ДЕЙСТВИЯ 3 МЕС. + 1 ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА.

ВРЕМЯ ПОСЕЩЕНИЯ В БУДНИ С 7.00 ДО 22.00, В ВЫХОДНЫЕ С 9.00 ДО 21.00

КОЛИЧЕСТВО ПОСЕЩЕНИЙ НЕ ОГРАНИЧЕНО В ТЕЧЕНИИ ВСЕГО ДЕЙСТВИЯ АБОНЕМЕНТА.', 90)
INSERT [dbo].[Abonement] ([id_abonement], [Name], [Stoimost], [Opisanie], [Srok_(days)]) VALUES (3, N'ФИТНЕС МЕСЯЦ', 2750.0000, N'СРОК ДЕЙСТВИЯ 1 МЕС. + 1 ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА.

ВРЕМЯ ПОСЕЩЕНИЯ В БУДНИ С 7.00 ДО 22.00, В ВЫХОДНЫЕ С 9.00 ДО 21.00

КОЛИЧЕСТВО ПОСЕЩЕНИЙ НЕ ОГРАНИЧЕНО В ТЕЧЕНИЕ ВСЕГО ДЕЙСТВИЯ АБОНЕМЕНТА.', 30)
INSERT [dbo].[Abonement] ([id_abonement], [Name], [Stoimost], [Opisanie], [Srok_(days)]) VALUES (4, N'ДЕТСКАЯ', 1000.0000, N'ДЕТИ ДО 16 ЛЕТ.', 30)
INSERT [dbo].[Abonement] ([id_abonement], [Name], [Stoimost], [Opisanie], [Srok_(days)]) VALUES (5, N'1 ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА', 600.0000, N'ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА.', 1)
SET IDENTITY_INSERT [dbo].[Abonement] OFF
GO
SET IDENTITY_INSERT [dbo].[Klient] ON 

INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (2, N'Дунин-Барковский Герман Онуфриевич ', CAST(N'1996-03-20' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (3, N'Беломестова Лидия Захаровна ', CAST(N'1959-02-08' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (4, N'Ячменцев Роман Серафимович ', CAST(N'1989-09-30' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (5, N'Кадцына Клара Трофимовна ', CAST(N'1991-01-01' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (6, N'Натарова Лада Ираклиевна ', CAST(N'1984-04-12' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (7, N'Карамзин Ким Никанорович ', CAST(N'1979-10-30' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (8, N'Кумовьева Берта Павеловна ', CAST(N'1986-01-11' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (9, N'Ямлиханова Наталия Афанасиевна ', CAST(N'1987-04-13' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (10, N'Лапухина Людмила Брониславовна ', CAST(N'1971-06-18' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (11, N'Рыбаков Руслан Никонович ', CAST(N'1981-04-01' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (12, N'Бурмакин Матвей Саввевич ', CAST(N'2006-01-18' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (13, N'Берёзкина ?Агата Петровна ', CAST(N'1960-01-22' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (14, N'Бока Аза Филипповна ', CAST(N'1975-02-03' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (15, N'Набокина Лилия Глебовна ', CAST(N'1962-03-06' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (16, N'Ковшутина Юлия Степановна ', CAST(N'2003-02-22' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (17, N'Бабина Елена Данииловна ', CAST(N'1988-02-16' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (18, N'Радченко Вероника Ираклиевна ', CAST(N'1960-11-20' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (19, N'Кадетов Клавдий Мартьянович ', CAST(N'1989-10-01' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (20, N'Круглова Оксана Глебовна ', CAST(N'1965-11-19' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (21, N'Хромченко Олег Панкратиевич ', CAST(N'2002-07-09' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (22, N'Ворожцов Афанасий Владиславович ', CAST(N'1969-04-30' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (23, N'Яламов Афанасий Архипович ', CAST(N'1964-12-24' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (24, N'Фокина Тамара Борисовна ', CAST(N'1975-05-01' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (25, N'Кувардина Наталия Ипполитовна ', CAST(N'2009-03-06' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (26, N'Сигов Захар Вадимович ', CAST(N'1983-10-18' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (27, N'Беломестный Вячеслав Капитонович ', CAST(N'1994-01-01' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (28, N'Горохин Николай Олегович ', CAST(N'1993-01-13' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (29, N'Комарова Алина Владиленовна ', CAST(N'1980-06-05' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (30, N'Овчинников Тимофей Эрнстович ', CAST(N'1993-06-22' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (31, N'Соловьева Галина Виталиевна ', CAST(N'1982-01-15' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (32, N'Толмачёва Арина Леонидовна ', CAST(N'1988-10-14' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (33, N'Грефа Любава Павеловна ', CAST(N'1985-03-24' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (34, N'Курбанов Мирон Тихонович ', CAST(N'1994-09-02' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (35, N'Яблокова Мирослава Тимофеевна ', CAST(N'1991-09-16' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (36, N'Павленко Тимур Евлампиевич ', CAST(N'1989-04-26' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (37, N'Потапов Онуфрий Игнатиевич ', CAST(N'1996-09-10' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (38, N'Жеффре Геннадий Андроникович ', CAST(N'1997-08-02' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (39, N'Янов Валерий Всеволодович ', CAST(N'1968-07-05' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (40, N'Яковлев Чеслав Демьянович ', CAST(N'1989-04-11' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (41, N'Кризько Афанасий Глебович ', CAST(N'1982-04-30' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (42, N'Палагута Платон Денисович ', CAST(N'1988-09-19' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (43, N'Платонов Потап Проклович ', CAST(N'1994-08-21' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (44, N'Пушной Кирилл Елисеевич ', CAST(N'1974-01-06' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (45, N'Баринова Рада Никитевна ', CAST(N'1992-04-25' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (46, N'Витаева Алина Емельяновна ', CAST(N'1980-06-11' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (47, N'Зыков Остап Эдуардович ', CAST(N'1990-10-19' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (48, N'Кузаев Никанор Александрович ', CAST(N'1994-04-28' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (49, N'Рашета Светлана Игоревна ', CAST(N'1988-07-31' AS Date))
INSERT [dbo].[Klient] ([id_klient], [FIO], [Data_rozhdeniya]) VALUES (50, N'Трындин Прокл Евгениевич', CAST(N'1992-04-27' AS Date))
SET IDENTITY_INSERT [dbo].[Klient] OFF
GO
SET IDENTITY_INSERT [dbo].[Klub_Karta] ON 

INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (1, 14, 2, CAST(N'2019-01-01' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (2, 36, 3, CAST(N'2019-01-01' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (3, 2, 1, CAST(N'2019-01-08' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (4, 11, 3, CAST(N'2019-01-14' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (5, 22, 3, CAST(N'2019-01-14' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (6, 33, 3, CAST(N'2019-01-14' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (7, 49, 3, CAST(N'2019-01-14' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (8, 50, 1, CAST(N'2019-01-15' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (9, 16, 1, CAST(N'2019-01-16' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (10, 38, 5, CAST(N'2019-01-16' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (12, 5, 3, CAST(N'2019-02-03' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (13, 3, 1, CAST(N'2019-02-21' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (14, 24, 1, CAST(N'2019-02-21' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (15, 8, 3, CAST(N'2019-03-15' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (16, 7, 3, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (17, 29, 2, CAST(N'2019-03-22' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (18, 45, 3, CAST(N'2019-03-22' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (19, 17, 3, CAST(N'2019-03-28' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (20, 39, 5, CAST(N'2019-03-28' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (21, 6, 3, CAST(N'2019-04-04' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (22, 18, 3, CAST(N'2019-04-06' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (23, 40, 5, CAST(N'2019-04-06' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (24, 27, 2, CAST(N'2019-04-15' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (25, 43, 2, CAST(N'2019-04-15' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (26, 12, 4, CAST(N'2019-04-18' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (27, 23, 3, CAST(N'2019-04-18' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (28, 34, 1, CAST(N'2019-04-18' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (29, 20, 3, CAST(N'2019-04-21' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (30, 42, 5, CAST(N'2019-04-21' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (31, 9, 2, CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (32, 31, 3, CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (33, 47, 5, CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (34, 4, 2, CAST(N'2019-05-07' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (35, 25, 4, CAST(N'2019-05-07' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (36, 13, 3, CAST(N'2019-05-23' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (37, 35, 1, CAST(N'2019-05-23' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (38, 19, 5, CAST(N'2019-06-01' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (39, 41, 1, CAST(N'2019-06-01' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (40, 26, 2, CAST(N'2019-06-04' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (41, 28, 1, CAST(N'2019-06-04' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (42, 44, 5, CAST(N'2019-06-04' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (43, 15, 2, CAST(N'2019-06-08' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (44, 37, 2, CAST(N'2019-06-08' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (45, 10, 3, CAST(N'2019-06-10' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (46, 21, 5, CAST(N'2019-06-10' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (47, 32, 3, CAST(N'2019-06-10' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (48, 48, 2, CAST(N'2019-06-10' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (49, 30, 3, CAST(N'2019-07-01' AS Date))
INSERT [dbo].[Klub_Karta] ([id_klub], [id_klient_FK], [id_abonement_FK], [Data_nachala]) VALUES (50, 46, 5, CAST(N'2019-07-01' AS Date))
SET IDENTITY_INSERT [dbo].[Klub_Karta] OFF
GO
ALTER TABLE [dbo].[Klub_Karta]  WITH CHECK ADD  CONSTRAINT [FK_Klub_Karta_Abonement] FOREIGN KEY([id_abonement_FK])
REFERENCES [dbo].[Abonement] ([id_abonement])
GO
ALTER TABLE [dbo].[Klub_Karta] CHECK CONSTRAINT [FK_Klub_Karta_Abonement]
GO
ALTER TABLE [dbo].[Klub_Karta]  WITH CHECK ADD  CONSTRAINT [FK_Klub_Karta_Klient] FOREIGN KEY([id_klient_FK])
REFERENCES [dbo].[Klient] ([id_klient])
GO
ALTER TABLE [dbo].[Klub_Karta] CHECK CONSTRAINT [FK_Klub_Karta_Klient]
GO
USE [master]
GO
ALTER DATABASE [dbFitnessClub] SET  READ_WRITE 
GO
