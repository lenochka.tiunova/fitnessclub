﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessClubApp
{
    public partial class FormNewClubCard : Form
    {
        public FormNewClubCard()
        {
            InitializeComponent();
        }

        dbFitnessClubEntities db = new dbFitnessClubEntities();

        private void btnSave_Click(object sender, EventArgs e)
        {
            Klub_Karta klub_Karta = new Klub_Karta
            {
                id_klient_FK = db.Klients.FirstOrDefault(n => n.FIO == cbNameCustomers.Text).id_klient,
                id_abonement_FK = db.Abonements.FirstOrDefault(n => n.Name == cbSubscription.Text).id_abonement,
                Data_nachala = dtpDateStart.Value
            };
            db.Klub_Karta.Add(klub_Karta);
            db.SaveChanges();
            MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            exit = true;
            this.Close();
        }

        private void FormNewClubCard_Load(object sender, EventArgs e)
        {
            cbSubscription.DataSource = db.Abonements.Select(n => n.Name).ToList();
            cbNameCustomers.DataSource = db.Klients.Select(n => n.FIO).ToList();
        }

        public bool exit = false;
        private void FormNewClubCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
