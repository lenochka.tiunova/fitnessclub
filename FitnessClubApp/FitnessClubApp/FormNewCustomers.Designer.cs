﻿
namespace FitnessClubApp
{
    partial class FormNewCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNameCustomers = new System.Windows.Forms.Label();
            this.lblDateBirth = new System.Windows.Forms.Label();
            this.tbNameCustomers = new System.Windows.Forms.TextBox();
            this.dtpDateBirth = new System.Windows.Forms.DateTimePicker();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNameCustomers
            // 
            this.lblNameCustomers.AutoSize = true;
            this.lblNameCustomers.Location = new System.Drawing.Point(64, 31);
            this.lblNameCustomers.Name = "lblNameCustomers";
            this.lblNameCustomers.Size = new System.Drawing.Size(37, 13);
            this.lblNameCustomers.TabIndex = 0;
            this.lblNameCustomers.Text = "ФИО:";
            // 
            // lblDateBirth
            // 
            this.lblDateBirth.AutoSize = true;
            this.lblDateBirth.Location = new System.Drawing.Point(12, 86);
            this.lblDateBirth.Name = "lblDateBirth";
            this.lblDateBirth.Size = new System.Drawing.Size(89, 13);
            this.lblDateBirth.TabIndex = 1;
            this.lblDateBirth.Text = "Дата рождения:";
            // 
            // tbNameCustomers
            // 
            this.tbNameCustomers.Location = new System.Drawing.Point(107, 28);
            this.tbNameCustomers.MaxLength = 100;
            this.tbNameCustomers.Multiline = true;
            this.tbNameCustomers.Name = "tbNameCustomers";
            this.tbNameCustomers.Size = new System.Drawing.Size(301, 39);
            this.tbNameCustomers.TabIndex = 2;
            // 
            // dtpDateBirth
            // 
            this.dtpDateBirth.Location = new System.Drawing.Point(107, 84);
            this.dtpDateBirth.Name = "dtpDateBirth";
            this.dtpDateBirth.Size = new System.Drawing.Size(301, 20);
            this.dtpDateBirth.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Moccasin;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(177, 132);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FormNewCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(420, 175);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpDateBirth);
            this.Controls.Add(this.tbNameCustomers);
            this.Controls.Add(this.lblDateBirth);
            this.Controls.Add(this.lblNameCustomers);
            this.Name = "FormNewCustomers";
            this.Text = "Клиент. Добавление";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNewCustomers_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNameCustomers;
        private System.Windows.Forms.Label lblDateBirth;
        private System.Windows.Forms.TextBox tbNameCustomers;
        private System.Windows.Forms.DateTimePicker dtpDateBirth;
        private System.Windows.Forms.Button btnSave;
    }
}