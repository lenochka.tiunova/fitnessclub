﻿
namespace FitnessClubApp
{
    partial class mainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblSearch = new System.Windows.Forms.Label();
            this.dgvFitnessClub = new System.Windows.Forms.DataGridView();
            this.menuProductShop = new System.Windows.Forms.MenuStrip();
            this.miTables = new System.Windows.Forms.ToolStripMenuItem();
            this.miCustomers = new System.Windows.Forms.ToolStripMenuItem();
            this.miSubscription = new System.Windows.Forms.ToolStripMenuItem();
            this.miClubCard = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.miWord = new System.Windows.Forms.ToolStripMenuItem();
            this.miExel = new System.Windows.Forms.ToolStripMenuItem();
            this.miChart = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery2 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFitnessClub)).BeginInit();
            this.menuProductShop.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(54, 32);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(733, 20);
            this.tbSearch.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Location = new System.Drawing.Point(803, 32);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 20);
            this.btnSearch.TabIndex = 9;
            this.btnSearch.Text = "Найти";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(5, 35);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(39, 13);
            this.lblSearch.TabIndex = 8;
            this.lblSearch.Text = "Поиск";
            // 
            // dgvFitnessClub
            // 
            this.dgvFitnessClub.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFitnessClub.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            this.dgvFitnessClub.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvFitnessClub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFitnessClub.Location = new System.Drawing.Point(3, 61);
            this.dgvFitnessClub.Name = "dgvFitnessClub";
            this.dgvFitnessClub.Size = new System.Drawing.Size(895, 471);
            this.dgvFitnessClub.TabIndex = 7;
            this.dgvFitnessClub.TabStop = false;
            // 
            // menuProductShop
            // 
            this.menuProductShop.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuProductShop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTables,
            this.miEditMenu,
            this.miExport,
            this.miChart,
            this.miQuery});
            this.menuProductShop.Location = new System.Drawing.Point(0, 0);
            this.menuProductShop.Name = "menuProductShop";
            this.menuProductShop.Size = new System.Drawing.Size(898, 24);
            this.menuProductShop.TabIndex = 6;
            // 
            // miTables
            // 
            this.miTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCustomers,
            this.miSubscription,
            this.miClubCard});
            this.miTables.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.miTables.Name = "miTables";
            this.miTables.Size = new System.Drawing.Size(68, 20);
            this.miTables.Text = "Таблицы";
            // 
            // miCustomers
            // 
            this.miCustomers.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miCustomers.Name = "miCustomers";
            this.miCustomers.Size = new System.Drawing.Size(153, 22);
            this.miCustomers.Text = "Клиенты";
            this.miCustomers.Click += new System.EventHandler(this.miCustomers_Click);
            // 
            // miSubscription
            // 
            this.miSubscription.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miSubscription.Name = "miSubscription";
            this.miSubscription.Size = new System.Drawing.Size(153, 22);
            this.miSubscription.Text = "Абонементы";
            this.miSubscription.Click += new System.EventHandler(this.miSubscription_Click);
            // 
            // miClubCard
            // 
            this.miClubCard.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miClubCard.Name = "miClubCard";
            this.miClubCard.Size = new System.Drawing.Size(153, 22);
            this.miClubCard.Text = "Клубная карта";
            this.miClubCard.Click += new System.EventHandler(this.miClubCard_Click);
            // 
            // miEditMenu
            // 
            this.miEditMenu.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miEditMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miEdit,
            this.miDelete});
            this.miEditMenu.Name = "miEditMenu";
            this.miEditMenu.Size = new System.Drawing.Size(108, 20);
            this.miEditMenu.Text = "Редактирование";
            // 
            // miAdd
            // 
            this.miAdd.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miAdd.Enabled = false;
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(180, 22);
            this.miAdd.Text = "Добавить";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miEdit
            // 
            this.miEdit.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miEdit.Enabled = false;
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(180, 22);
            this.miEdit.Text = "Редактировать";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.BackColor = System.Drawing.SystemColors.ControlLight;
            this.miDelete.Enabled = false;
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(180, 22);
            this.miDelete.Text = "Удалить";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miExport
            // 
            this.miExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWord,
            this.miExel});
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(64, 20);
            this.miExport.Text = "Экспорт";
            // 
            // miWord
            // 
            this.miWord.Name = "miWord";
            this.miWord.Size = new System.Drawing.Size(103, 22);
            this.miWord.Text = "Word";
            this.miWord.Click += new System.EventHandler(this.miWord_Click);
            // 
            // miExel
            // 
            this.miExel.Name = "miExel";
            this.miExel.Size = new System.Drawing.Size(103, 22);
            this.miExel.Text = "Excel";
            this.miExel.Click += new System.EventHandler(this.miExel_Click);
            // 
            // miChart
            // 
            this.miChart.Name = "miChart";
            this.miChart.Size = new System.Drawing.Size(82, 20);
            this.miChart.Text = "Диаграмма";
            this.miChart.Click += new System.EventHandler(this.miChart_Click);
            // 
            // miQuery
            // 
            this.miQuery.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miQuery1,
            this.miQuery2});
            this.miQuery.Name = "miQuery";
            this.miQuery.Size = new System.Drawing.Size(68, 20);
            this.miQuery.Text = "Запросы";
            // 
            // miQuery1
            // 
            this.miQuery1.Name = "miQuery1";
            this.miQuery1.Size = new System.Drawing.Size(180, 22);
            this.miQuery1.Text = "Запрос №1";
            this.miQuery1.Click += new System.EventHandler(this.miQuery1_Click);
            // 
            // miQuery2
            // 
            this.miQuery2.Name = "miQuery2";
            this.miQuery2.Size = new System.Drawing.Size(180, 22);
            this.miQuery2.Text = "Запрос №2";
            this.miQuery2.Click += new System.EventHandler(this.miQuery2_Click);
            // 
            // saveWord
            // 
            this.saveWord.Filter = "Документ word|*.docx";
            this.saveWord.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWord_FileOk);
            // 
            // saveExcel
            // 
            this.saveExcel.Filter = "Книга Excel|*.xlsx";
            this.saveExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.saveExcel_FileOk);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(898, 529);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.dgvFitnessClub);
            this.Controls.Add(this.menuProductShop);
            this.Name = "mainForm";
            this.Text = "Фитнес клуб";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.mainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFitnessClub)).EndInit();
            this.menuProductShop.ResumeLayout(false);
            this.menuProductShop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.DataGridView dgvFitnessClub;
        private System.Windows.Forms.MenuStrip menuProductShop;
        public System.Windows.Forms.ToolStripMenuItem miTables;
        public System.Windows.Forms.ToolStripMenuItem miCustomers;
        public System.Windows.Forms.ToolStripMenuItem miSubscription;
        public System.Windows.Forms.ToolStripMenuItem miClubCard;
        public System.Windows.Forms.ToolStripMenuItem miEditMenu;
        private System.Windows.Forms.ToolStripMenuItem miAdd;
        public System.Windows.Forms.ToolStripMenuItem miEdit;
        public System.Windows.Forms.ToolStripMenuItem miDelete;
        public System.Windows.Forms.ToolStripMenuItem miExport;
        private System.Windows.Forms.ToolStripMenuItem miWord;
        private System.Windows.Forms.ToolStripMenuItem miExel;
        private System.Windows.Forms.ToolStripMenuItem miChart;
        private System.Windows.Forms.ToolStripMenuItem miQuery;
        private System.Windows.Forms.ToolStripMenuItem miQuery1;
        private System.Windows.Forms.ToolStripMenuItem miQuery2;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.SaveFileDialog saveExcel;
    }
}

