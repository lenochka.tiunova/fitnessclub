﻿
namespace FitnessClubApp
{
    partial class FormNewClubCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpDateStart = new System.Windows.Forms.DateTimePicker();
            this.lblDateStart = new System.Windows.Forms.Label();
            this.lblNameCustomers = new System.Windows.Forms.Label();
            this.cbNameCustomers = new System.Windows.Forms.ComboBox();
            this.cbSubscription = new System.Windows.Forms.ComboBox();
            this.lblSubscription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Moccasin;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(186, 162);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpDateStart
            // 
            this.dtpDateStart.Location = new System.Drawing.Point(151, 110);
            this.dtpDateStart.Name = "dtpDateStart";
            this.dtpDateStart.Size = new System.Drawing.Size(336, 20);
            this.dtpDateStart.TabIndex = 8;
            // 
            // lblDateStart
            // 
            this.lblDateStart.AutoSize = true;
            this.lblDateStart.Location = new System.Drawing.Point(9, 116);
            this.lblDateStart.Name = "lblDateStart";
            this.lblDateStart.Size = new System.Drawing.Size(124, 13);
            this.lblDateStart.TabIndex = 6;
            this.lblDateStart.Text = "Дата начала действия:";
            // 
            // lblNameCustomers
            // 
            this.lblNameCustomers.AutoSize = true;
            this.lblNameCustomers.Location = new System.Drawing.Point(57, 37);
            this.lblNameCustomers.Name = "lblNameCustomers";
            this.lblNameCustomers.Size = new System.Drawing.Size(76, 13);
            this.lblNameCustomers.TabIndex = 5;
            this.lblNameCustomers.Text = "Имя клиента:";
            // 
            // cbNameCustomers
            // 
            this.cbNameCustomers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameCustomers.FormattingEnabled = true;
            this.cbNameCustomers.Location = new System.Drawing.Point(151, 34);
            this.cbNameCustomers.Name = "cbNameCustomers";
            this.cbNameCustomers.Size = new System.Drawing.Size(336, 21);
            this.cbNameCustomers.TabIndex = 10;
            // 
            // cbSubscription
            // 
            this.cbSubscription.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSubscription.FormattingEnabled = true;
            this.cbSubscription.Location = new System.Drawing.Point(151, 74);
            this.cbSubscription.Name = "cbSubscription";
            this.cbSubscription.Size = new System.Drawing.Size(336, 21);
            this.cbSubscription.TabIndex = 11;
            // 
            // lblSubscription
            // 
            this.lblSubscription.AutoSize = true;
            this.lblSubscription.Location = new System.Drawing.Point(9, 77);
            this.lblSubscription.Name = "lblSubscription";
            this.lblSubscription.Size = new System.Drawing.Size(124, 13);
            this.lblSubscription.TabIndex = 12;
            this.lblSubscription.Text = "Название абонемента:";
            // 
            // FormNewClubCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(515, 208);
            this.Controls.Add(this.lblSubscription);
            this.Controls.Add(this.cbSubscription);
            this.Controls.Add(this.cbNameCustomers);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpDateStart);
            this.Controls.Add(this.lblDateStart);
            this.Controls.Add(this.lblNameCustomers);
            this.Name = "FormNewClubCard";
            this.Text = "Клубная карта. Добавление";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNewClubCard_FormClosing);
            this.Load += new System.EventHandler(this.FormNewClubCard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpDateStart;
        private System.Windows.Forms.Label lblDateStart;
        private System.Windows.Forms.Label lblNameCustomers;
        private System.Windows.Forms.ComboBox cbNameCustomers;
        private System.Windows.Forms.ComboBox cbSubscription;
        private System.Windows.Forms.Label lblSubscription;
    }
}