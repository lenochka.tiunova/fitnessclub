﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace FitnessClubApp
{
    public partial class FormSubscriptionEdit : Form
    {
        public FormSubscriptionEdit()
        {
            InitializeComponent();
        }

        dbFitnessClubEntities db = new dbFitnessClubEntities();

        public int index = -1;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbName.Text) && !String.IsNullOrEmpty(tbDescription.Text))
            {
                var abonement = db.Abonements.FirstOrDefault(n => n.id_abonement == index);
                abonement.Name = tbName.Text;
                abonement.Stoimost = (int)nudPrice.Value;
                abonement.Opisanie = tbDescription.Text;
                abonement.Srok__days_ = (int)nudTimeDays.Value;
                db.SaveChanges();
                MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FormSubscriptionEdit_Load(object sender, EventArgs e)
        {
            tbName.Text = db.Abonements.FirstOrDefault(n => n.id_abonement == index).Name;
            nudPrice.Value = (decimal)db.Abonements.FirstOrDefault(n => n.id_abonement == index).Stoimost;
            tbDescription.Text = db.Abonements.FirstOrDefault(n => n.id_abonement == index).Opisanie;
            nudTimeDays.Value = (decimal)db.Abonements.FirstOrDefault(n => n.id_abonement == index).Srok__days_;
        }

        public bool exit = false;

        private void FormSubscriptionEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
