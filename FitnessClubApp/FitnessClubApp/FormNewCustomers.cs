﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessClubApp
{
    public partial class FormNewCustomers : Form
    {
        public FormNewCustomers()
        {
            InitializeComponent();
            dtpDateBirth.Value = new DateTime(DateTime.Now.AddYears(-20).Year, 01, 01);
            dtpDateBirth.MaxDate = DateTime.Now.AddYears(-7).AddDays(-1);
        }

        dbFitnessClubEntities db = new dbFitnessClubEntities();

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbNameCustomers.Text))
            {
                Klient klient = new Klient
                {
                    FIO = tbNameCustomers.Text,
                    Data_rozhdeniya = dtpDateBirth.Value
                };
                db.Klients.Add(klient);
                db.SaveChanges();
                MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public bool exit = false;
        private void FormNewCustomers_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
